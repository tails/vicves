[[!meta title="Contributing to Tails"]]

There are many ways you can contribute to Tails. No effort is too
small and whatever you bring to this community will be appreciated.
So read on to find out how you can make a difference in Tails.

<div class="contribute-roles-1">
<h2>User</h2>
<div class="contribute-role" id="user">
  <img src="icon_419.svg" />
  <p>Every user can help others or provide developers with useful information.</p>
  <ul>
    <li>[[Report bugs|doc/first_steps/bug_reporting]]</li>
    <li>[[Provide needed input to developers|contribute/how/input]]</li>
    <li>[[Help other Tails users|contribute/how/help]]</li>
  </ul>
</div>
</div>

<div class="contribute-roles-1">
<h2>Donate</h2>
<div class="contribute-role" id="donate">
  <img src="icon_6409.svg" />
  <img src="icon_19538.svg" />
  <img src="icon_285.svg" />
  <img src="icon_21216.svg" />
  <p>Donating speeds up the development of Tails.</p>
  <ul>
    <li>[[Make a donation|contribute/how/donate]]</li>
  </ul>
</div>
</div>

<div class="contribute-roles-3">
<h2>Contribute with your language skills</h2>
<div class="contribute-role" id="content-writer">
  <h3>Writer</h3>
  <img src="icon_18907.svg" />
  <img src="icon_26704.svg" />
  <img src="icon_1520.svg" />
  <img src="icon_15262.svg" />
  <p>Good writers can make Tails accessible to more people.</p>
  <ul>
    <li>[[Improve documentation|contribute/how/documentation]]</li>
    <li>[[Write press releases|contribute/how/promote]]</li>
  </ul>
</div>
<div class="contribute-role" id="translator">
  <h3>Translator</h3>
  <img src="icon_5735.svg" />
  <img src="icon_987.svg" />
  <img src="icon_24074.svg" />
  <p>Translators can allow more people around the world to use Tails.</p>
  <ul>
    <li>[[Improve Tails in your own language|contribute/how/translate]]</li>
  </ul>
</div>
<div class="contribute-role" id="speaker">
  <h3>Speaker</h3>
  <img src="icon_1186_cut.svg" />
  <img src="icon_1186.svg" />
  <p>Speakers can advocate Tails to all kinds of public.</p>
  <ul>
    <li>[[Talk at events|contribute/how/promote]]</li>
  </ul>
</div>
</div>

<div class="contribute-roles-3">
<h2>Contribute with your technical skills</h2>
<div class="contribute-role" id="developer">
  <h3>Software developer</h3>
  <img src="icon_18033.svg" />
  <img src="icon_21297.svg" />
  <p>Developers with very diverse skills can improve Tails code.</p>
  <ul>
    <li>[[Fix a bug|contribute/how/code]]</li>
    <li>[[Implement a feature|contribute/how/code]]</li>
  </ul>
</div>
<div class="contribute-role" id="sysadmin">
  <h3>System administrator</h3>
  <img src="icon_8949.svg" />
  <img src="icon_4684.svg" />
  <img src="icon_14182.svg" />
  <p>System administrators can contribute to the infrastructure behind Tails.</p>
  <ul>
    <li>[[Run a BitTorrent seed|contribute/how/mirror]]</li>
    <li>[[Run a HTTP mirror|contribute/how/mirror]]</li>
  </ul>
</div>
<div class="contribute-role" id="designer">
  <img src="icon_8499.svg" />
  <img src="icon_6552.svg" />
  <img src="icon_14005.svg" />
  <img src="icon_18203.svg" />
  <h3>Designer</h3>
  <p>Designers can make Tails easier to use and more appealing.</p>
  <ul>
    <li>[[Improve the website|contribute/how/website]]</li>
    <li>[[Design graphics|contribute/how/graphics]]</li>
    <li>[[Improve the Tails user interface|contribute/how/user_interface]]</li>
  </ul>
</div>
</div>
