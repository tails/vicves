[[!meta title="Contributing to Tails"]]

There are many ways you can contribute to Tails. No effort is too
small and whatever you bring to this community will be appreciated.
So read on to find out how you can make a difference in Tails.

<div class="contribute-roles-1">
<h2>User</h2>
<div class="contribute-role" id="user">
  <p>Every user can help others or provide developers with useful information.</p>
  <ul>
    <li>[[Report bugs|doc/first_steps/bug_reporting]]</li>
    <li>[[Provide needed input to developers|contribute/how/input]]</li>
    <li>[[Help other Tails users|contribute/how/help]]</li>
  </ul>
</div>
</div>

<div class="contribute-roles-1">
<h2>Donate</h2>
<div class="contribute-role" id="donate">
  <p>Donating speeds up the development of Tails.</p>
  <ul>
    <li>[[Make a donation|contribute/how/donate]]</li>
  </ul>
</div>
</div>

<div class="contribute-roles-3">
<h2>Contribute with your language skills</h2>
<div class="contribute-role" id="content-writer">
  <h3>Writer</h3>
  <p>Good writers can make Tails accessible to more people.</p>
  <ul>
    <li>[[Improve documentation|contribute/how/documentation]]</li>
    <li>[[Write press releases|contribute/how/promote]]</li>
  </ul>
</div>
<div class="contribute-role" id="translator">
  <h3>Translator</h3>
  <p>Translators can allow more people around the world to use Tails.</p>
  <ul>
    <li>[[Improve Tails in your own language|contribute/how/translate]]</li>
  </ul>
</div>
<div class="contribute-role" id="speaker">
  <h3>Speaker</h3>
  <p>Speakers can advocate Tails to all kinds of public.</p>
  <ul>
    <li>[[Talk at events|contribute/how/promote]]</li>
  </ul>
</div>
</div>

<div class="contribute-roles-3">
<h2>Contribute with your technical skills</h2>
<div class="contribute-role" id="developer">
  <h3>Software developer</h3>
  <p>Developers with very diverse skills can improve Tails code.</p>
  <ul>
    <li>[[Fix a bug|contribute/how/code]]</li>
    <li>[[Implement a feature|contribute/how/code]]</li>
  </ul>
</div>
<div class="contribute-role" id="sysadmin">
  <h3>System administrator</h3>
  <p>System administrators can contribute to the infrastructure behind Tails.</p>
  <ul>
    <li>[[Run a BitTorrent seed|contribute/how/mirror]]</li>
    <li>[[Run a HTTP mirror|contribute/how/mirror]]</li>
  </ul>
</div>
<div class="contribute-role" id="designer">
  <h3>Designer</h3>
  <p>Designers can make Tails easier to use and more appealing.</p>
  <ul>
    <li>[[Improve the website|contribute/how/website]]</li>
    <li>[[Design graphics|contribute/how/graphics]]</li>
    <li>[[Improve the Tails user interface|contribute/how/user_interface]]</li>
  </ul>
</div>
</div>

<div class="toc">

	<h1>Table of contents</h1>

	<ol>
		<li class="L2"><a href="#reference-documents">Reference documents</a></li>
		<li class="L2"><a href="#tools">Tools for contributors</a></li>
		<li class="L2"><a href="#release-cycle">Release cycle</a></li>
		<li class="L2"><a href="#upstream">Relationship with upstream</a></li>
		<li class="L2"><a href="#collective-process">Collective process</a></li>
		<li class="L2"><a href="#talk">Talk with us</a></li>
	</ol>

</div> <!-- .toc -->

<div class="note">

This section is only in English, because there are currently no way to
contribute to Tails if you do not understand English.

</div>

<a id="reference-documents"></a>

Reference documents
===================

  - [[Design documents|contribute/design]]
  - [[Blueprints|blueprint]]
  - [[Merge policy|contribute/merge_policy]]

<a id="tools"></a>

Tools for contributors
======================

  - Source code: [[Git repositories|contribute/git]]
  - [[Redmine bug tracker|contribute/working_together/Redmine]]
    - [Roadmap](https://labs.riseup.net/code/projects/tails/roadmap)
    - [[Easy tasks|easy_tasks]] for new contributors
    - Tasks by type of work:
        [code](https://labs.riseup.net/code/projects/tails/issues?query_id=119),
        [documentation](https://labs.riseup.net/code/projects/tails/issues?query_id=118),
        [website](https://labs.riseup.net/code/projects/tails/issues?query_id=115),
        [test](https://labs.riseup.net/code/projects/tails/issues?query_id=116),
        [sysadmin](https://labs.riseup.net/code/projects/tails/issues?query_id=113)
  - [[Building a Tails image|contribute/build]]
    - [[Build the website|contribute/build/website]]
    - [[Customize Tails|contribute/customize]]
  - [[Debian package builder|contribute/Debian_package_builder]], to automatically build our custom Debian packages
  - [[APT repository|contribute/APT_repository]], to store our custom Debian packages
  - [[FAQ for contributors|contribute/faq]]
  - [[Glossary for contributors|contribute/glossary]]

<a id="release-cycle"></a>

Release cycle
=============

  - [[Release schedule|contribute/release_schedule]]
  - [[Release process|contribute/release_process]]
    - [[Test suite|contribute/release_process/test]]

<a id="upstream"></a>

Relationship with upstream
==========================

  - [[Relationship with upstream|contribute/relationship_with_upstream]]
  - Bugs that we are interested in on the Debian BTS:
    [by usertag](http://udd.debian.org/cgi-bin/bts-usertags.cgi?user=tails-dev@boum.org),
    [by severity](http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=tails-dev@boum.org)

<a id="collective-process"></a>

Collective process
==================

  - [[Calendar|contribute/calendar]] of releases, meetings, working sessions, etc.
  - [[Meetings|contribute/meetings]], and minutes from past meetings
  - [[Marking a task as easy|contribute/working_together/criteria_for_easy_tasks]]
  - [[Document progress|contribute/working_together/document_progress]]
  - Roles
    - [[Front desk|contribute/working_together/roles/front_desk]]
    - [[Release manager|contribute/working_together/roles/release_manager]]
    - [[Ticket gardener|contribute/working_together/roles/ticket_gardener]]
    - [[Welcome and annoying nitpicker|contribute/working_together/roles/welcome_and_annoying_nitpicker]]

<a id="talk"></a>

Talk with us
============

  [[!inline pages="contribute/talk" raw="yes"]]
